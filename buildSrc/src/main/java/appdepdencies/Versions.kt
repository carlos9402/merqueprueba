package appdependencies

object Versions {

    const val legacy = "1.0.0"
    const val gradle = "4.0.1"
    const val kotlin = "1.4.10"
    const val kotlinGradle = "1.3.2"
    const val google = "4.3.3"
    const val navisafe = "2.1.0"
    const val koin = "2.1.6"

    const val room = "2.2.5"
    const val mockito = "3.1.0"
    const val mockio = "1.10.6"
    const val mockito_inline = "2.13.0"
    const val lifecycle = "2.2.0-rc03"
    const val savedstate = "1.0.0-rc01"

    const val retrofit = "2.7.1"

    const val okhttp3 = "4.3.1"

    const val appCompatX = "1.1.0"
    const val appCoreX = "1.1.0"
    const val constraintLayout = "1.1.3"
    const val material = "1.2.0-alpha01"
    const val viewPager2 = "1.0.0"
    const val swipeRefresh = "1.0.0"
    const val paging = "2.1.1"

    const val coil = "0.9.1"

    object Navigation {
        const val fragment = "2.2.0-rc02"
        const val ui = "2.2.0-rc02"
    }

    /* test */


    const val junit = "4.12"
    const val test_ext = "1.1.1"
    const val test_rules = "1.3.0"
    const val espresso = "3.3.0"



    const val toasty = "1.5.0"
    const val runner = "1.0.2"

}